using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanonRotation : MonoBehaviour
{
    public Vector3 _maxRotation;
    public Vector3 _minRotation;
    private float offset = -51.6f;
    public GameObject ShootPoint;
    public GameObject Bullet;
    public float ProjectileSpeed = 0;
    public float MaxSpeed;
    public float MinSpeed;
    public GameObject PotencyBar;
    private float initialScaleX;

    private void Awake()
    {
        _maxRotation = new Vector3(0, 0, 35);
        _minRotation = new Vector3(0, 0, -55);
        initialScaleX = PotencyBar.transform.localScale.x;
    }
    void Update()
    {
        var mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition); //guardem posici� de la c�mera
        var dist = mousePos - transform.position; //dist�ncia entre el click i la bala
        var ang = Mathf.Atan2(dist.y, dist.x) * 180f / Mathf.PI + offset;
        CheckRotation(ref ang);
        transform.rotation = Quaternion.Euler( 0, 0, ang);//angle que s'ha de rotar

        if(Input.GetMouseButton(0))
        {
            ProjectileSpeed += 0.001f; //cada frame s'ha de fer 4 cops m�s gran
        }
        if(Input.GetMouseButtonUp(0))
        {
            var projectile = Instantiate(Bullet, transform.parent); //On s'instancia?
            projectile.transform.position = ShootPoint.transform.position;
            projectile.GetComponent<Rigidbody2D>().velocity = new Vector2( 13 * (-(ang - 35) / 90) * ProjectileSpeed + 1 * -(ang - 35) / 90, 13 * ((ang + 55) / 90) * ProjectileSpeed + 1 * (ang + 55) / 90); //quina velocitat ha de tenir la bala? s'ha de fer alguna cosa al vector direcci�?
            ProjectileSpeed = 0;
            Destroy(projectile, 3f);
        }
        CalculateBarScale();
    }
    public void CalculateBarScale()
    {
        if (ProjectileSpeed >= 1)
        {
            ProjectileSpeed = 1;
        }

        PotencyBar.transform.localPosition = new Vector3( ProjectileSpeed - 1, 0, 0);
        /*
        PotencyBar.transform.localScale = new Vector3(Mathf.Lerp(0, initialScaleX, ProjectileSpeed / MaxSpeed),
            transform.localScale.y,
            transform.localScale.z);*/
    }
    private void CheckRotation(ref float ang)
    {
        if(ang > _maxRotation.z)
        {
            ang = _maxRotation.z;
        }
        if (ang < _minRotation.z)
        {
            ang = _minRotation.z;
        }
    }
}
